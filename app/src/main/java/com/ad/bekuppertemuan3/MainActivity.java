package com.ad.bekuppertemuan3;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.ad.bekuppertemuan3.Adapter.listAdapterCity;
import com.ad.bekuppertemuan3.Model.City;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.Thing;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity { //Tempat CALL APIService

    private APIService apiService;
    private ListView listView;

//    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiService = APIService.Factory.create();
        listView = (ListView) findViewById(R.id.listViewC);
        loadCity(); //manggil method
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void loadCity() {
        Call<City> cityList = apiService.getCityCall(); //variabel citylist untuk menampung list tadi
        cityList.enqueue(new Callback<City>() { // enqueue :request langsung nembak ke servernya
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
//                Log.d("MainActivity", response.body().getStatus());
                listAdapterCity listAdapter = new listAdapterCity(MainActivity.this, response.body().getData());
                listView.setAdapter(listAdapter);
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {

            }
        });
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
//        AppIndex.AppIndexApi.start(client, getIndexApiAction());
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        AppIndex.AppIndexApi.end(client, getIndexApiAction());
//        client.disconnect();
//    }
}
