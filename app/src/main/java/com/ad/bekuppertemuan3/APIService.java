package com.ad.bekuppertemuan3;


import com.ad.bekuppertemuan3.Model.City;
import com.ad.bekuppertemuan3.Model.Movie;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Amalia on 21/10/2016.
 */

public interface APIService {
    String UrlApi= "http://ibacor.com/api/";

    @GET("jadwal-bioskop") //alamat param
    Call<City> getCityCall();

    @GET  ("jadwal-biokop?id={id}")
    Call<Movie> getMovieCall(@Path("id") String id);//id jadi inputan


    public static class Factory{
        public static APIService create(){
            Retrofit retrofit= new Retrofit.Builder()
                    .baseUrl("http://ibacor.com/api/jadwal-bioskop/") //alamat utama yang gak ubah ubah
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();

            return retrofit.create(APIService.class);
        }
    }

}
