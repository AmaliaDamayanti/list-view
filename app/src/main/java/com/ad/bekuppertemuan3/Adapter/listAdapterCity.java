package com.ad.bekuppertemuan3.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ad.bekuppertemuan3.Model.City;
import com.ad.bekuppertemuan3.R;

import java.util.List;

/**
 * Created by Amalia on 21/10/2016.
 */

public class listAdapterCity extends BaseAdapter {

    private List<City.Data> city; //city:penamaan
    private Context context;
    private LayoutInflater inflater;

    public listAdapterCity(Context context, List<City.Data> city) {
        this.city = city;
        this.inflater = LayoutInflater.from(context); //ambil layout
        this.context = context;
    }

    @Override
    public int getCount() {
        return city.size();
    }

    @Override
    public Object getItem(int position) {
        return city.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if (inflater == null) { // membuat tampilan baru di atas tampilan layout utama, jadi analoginya seperti tumpukan layout.
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (view == null ){
            view= inflater.inflate(R.layout.item_list, null);
        }

        TextView tCity= (TextView) view.findViewById(R.id.textViewKota);

        final  String aCity= city.get(position).getKota();
        tCity.setText(aCity);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Kota : " + aCity + "\nID : " + city.get(position).getId(), Toast.LENGTH_SHORT).show();
            }
    });
        return view;

    }

}
