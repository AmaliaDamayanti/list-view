package com.ad.bekuppertemuan3.Model;

import java.util.List;

/**
 * Created by Amalia on 21/10/2016.
 */

public class Movie {

    public static class Data {
        private List<Data> data;

        public List<Data> getData() {
            return data;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }


        public static class Jadwal {
            private String bioskop;
            private List<String> jam;
            private String harga;

            public String getBioskop() {
                return bioskop;
            }

            public void setBioskop(String biokop) {
                this.bioskop = bioskop;
            }

            public List<String> getJam() {
                return jam;
            }

            public void setJam(List<String> jam) {
                this.jam = jam;
            }

            public String getHarga() {
                return harga;
            }

            public void setHarga(String harga) {
                this.harga = harga;
            }
        }


    }


}
